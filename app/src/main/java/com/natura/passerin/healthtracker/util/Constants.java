package com.natura.passerin.healthtracker.util;

/**
 * Created by passerin on 10/24/18.
 */

public class Constants {
    public static final String USER = "USER";
    public static final String NURSE_USER = "NURSE";
    public static final String PATIENT_USER = "PATIENT";
    public static final String USERNAME = "USERNAME";
    public static final String SELECTED_HIST = "SELECTEDHIST";
    public static final String FIELD_ERROR = "Please complete this";
    public static final String GENERIC_ERROR = "An error has ocurred please try again.";
    public static final String RISK_LOW = "LOW";
    public static final String RISK_HIGH = "HIGH";
    public static final String RISK_NOT = "NOT";
}
