package com.natura.passerin.healthtracker.fragments;

import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.natura.passerin.healthtracker.R;
import com.natura.passerin.healthtracker.activities.HomeActivity;
import com.natura.passerin.healthtracker.adapters.NurseHistRecyclerAdapter;
import com.natura.passerin.healthtracker.models.History;
import com.natura.passerin.healthtracker.util.Constants;
import com.natura.passerin.healthtracker.util.CustomProgressDialog;
import com.natura.passerin.healthtracker.util.GlobalContext;
import com.natura.passerin.healthtracker.util.Tools;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class NurseHomeFragment extends Fragment {

    ArrayList<History> mHistories = new ArrayList<History>();
    NurseHistRecyclerAdapter mAdapter;
    AppCompatActivity mActivity;
    CustomProgressDialog customProgressDialog;
    GlobalContext mGlobalContext;
    TextView tvEmpty;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_nurse_home, container, false);
        mActivity = (AppCompatActivity) getActivity();
        customProgressDialog = new CustomProgressDialog(mActivity);
        mGlobalContext = (GlobalContext) mActivity.getApplication();
        tvEmpty = (TextView) v.findViewById(R.id.tvEmpty);

        //sets an empty recyclerview for listing every history.
        RecyclerView rvRecycler = (RecyclerView) v.findViewById(R.id.rvRecycler);
        rvRecycler.setHasFixedSize(true);
        //sets linear layout for building the recycler view.
        final LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvRecycler.setLayoutManager(layoutManager);
        //builds an adapter for recyclerview
        mAdapter= new NurseHistRecyclerAdapter(mHistories,mActivity) {
            @Override
            public void onItemClick(History item) {
                //when an item is selected user is taken to detailfragment view
                mGlobalContext.put(Constants.SELECTED_HIST,item);
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                DetailsFragment detailsFragment = new DetailsFragment();
                ft.setCustomAnimations(R.anim.right_to_left_in, R.anim.left_to_right_out, R.anim.left_to_right_in, R.anim.right_to_left_out);
                ft.replace(R.id.container, detailsFragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.addToBackStack(null);
                ft.commit();
            }
        };
        rvRecycler.setAdapter(mAdapter);
        customProgressDialog.show();
        //every time this service's called the recycler widget is updated so user can
        //notice diferences in the content of the list.
        mGlobalContext.getRestApi().getHistoryList(new Callback<ArrayList<History>>() {
            @Override
            public void success(ArrayList<History> history, Response response) {
                customProgressDialog.dismiss();
                //if recovered list is 0 then we show an empty message.
                if(history.size() > 0) {
                    tvEmpty.setVisibility(View.GONE);
                }else{
                    tvEmpty.setVisibility(View.VISIBLE);
                }
                //clears the histories and re-adds them.
                mHistories.clear();
                mHistories.addAll(history);
                //notifies the adapter that data has changed
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError error) {
                customProgressDialog.dismiss();
                Tools.Logger.e(error.toString());
            }
        });

        //takes the user to the add history fragment.
        FloatingActionButton fab = v.findViewById(R.id.btAdd);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                AddHistoryFragment detailsFragment = new AddHistoryFragment();
                ft.setCustomAnimations(R.anim.slide_up, R.anim.slide_down,R.anim.slide_down, R.anim.slide_up);
                ft.replace(R.id.container, detailsFragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.addToBackStack(null);
                ft.commit();
            }
        });

        return v;
    }


}
