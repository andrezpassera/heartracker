package com.natura.passerin.healthtracker.models;

/**
 * Created by passerin on 10/23/18.
 */

public class History {
    String idHistory;
    String name;
    String age;
    String bloodPreassure;
    String heartRate;
    String date;

    public History(String idHistory, String name, String age, String bloodPreassure, String heartRate, String date) {
        this.idHistory = idHistory;
        this.name = name;
        this.age = age;
        this.bloodPreassure = bloodPreassure;
        this.heartRate = heartRate;
        this.date = date;
    }

    public String getIdHistory() {
        return idHistory;
    }

    public void setIdHistory(String idHistory) {
        this.idHistory = idHistory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getBloodPreassure() {
        return bloodPreassure;
    }

    public void setBloodPreassure(String bloodPreassure) {
        this.bloodPreassure = bloodPreassure;
    }

    public String getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(String heartRate) {
        this.heartRate = heartRate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
