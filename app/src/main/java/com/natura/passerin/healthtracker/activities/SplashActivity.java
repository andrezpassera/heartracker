package com.natura.passerin.healthtracker.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import com.natura.passerin.healthtracker.R;
import com.natura.passerin.healthtracker.util.Tools;

public class SplashActivity extends AppCompatActivity {
    private static int SLEEP_TIME = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash_screen);

        //sets app version on screen
        try {
            TextView tvVersion = (TextView) findViewById(R.id.tvversionName);
            String versionName = SplashActivity.this.getPackageManager()
                    .getPackageInfo(SplashActivity.this.getPackageName(), 0).versionName;
            if (versionName != null) {
                tvVersion.setText("v"+versionName);
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
            SplashWaiter launcher = new SplashWaiter();
            launcher.start();

    }

        private class SplashWaiter extends Thread {
            @Override
            public void run() {
                try {
                    Thread.sleep(SLEEP_TIME * 500);
                } catch (Exception e) {
                    Tools.Logger.e("Ocurrio un error en el Splash");
                }
                //after 5 seconds you're taken to login activity.
                Intent intent = new Intent(SplashActivity.this,LoginActivity.class);
                startActivity(intent);
                SplashActivity.this.finish();
            }
        }
}
