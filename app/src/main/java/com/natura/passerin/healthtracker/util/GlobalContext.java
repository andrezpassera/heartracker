package com.natura.passerin.healthtracker.util;


import android.app.Application;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.natura.passerin.healthtracker.comm.CommReq;
import com.natura.passerin.healthtracker.comm.OkClient;
import com.natura.passerin.healthtracker.comm.RestApi;
import io.fabric.sdk.android.Fabric;
import java.util.HashMap;
import retrofit.RestAdapter;

/**
 * Created by passerin on 10/24/18.
 */

public class GlobalContext extends Application {

    OkClient okClient;
    HashMap<String,Object> dataBundle = null;
    RestAdapter restAdapter;
    RestApi restApi;
    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        dataBundle = new HashMap<String,Object>();
        okClient = new OkClient();

        restAdapter = new RestAdapter.Builder()
                .setEndpoint(CommReq.BASE_URL)
                .setClient(okClient)
                .setLogLevel(RestAdapter.LogLevel.FULL).
                        setLog(new RestAdapter.Log() {
                            @Override
                            public void log(String msg) {
                                Tools.Logger.i("Retrofit -> " + msg);
                            }
                        })
                .build();
        restApi = restAdapter.create(RestApi.class);
    }

    public RestApi getRestApi() {

        if (restAdapter==null) {

            restAdapter = new RestAdapter.Builder()
                    .setEndpoint(CommReq.BASE_URL)
                    .setClient(okClient)
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .setLog(new RestAdapter.Log() {
                        @Override
                        public void log(String msg) {
                            Tools.Logger.i("Retrofit -> " + msg);
                        }
                    })
                    .build();
            restApi = restAdapter.create(RestApi.class);
        }
        return restApi;
    }

    public void put(String key, Object value){
        this.dataBundle.put(key, value);
        Log.d("BIG BAG", "Size: " + this.dataBundle.size());
    }

    public <T> T get(String key, Class<T> cls){
        if (cls.isInstance(this.dataBundle.get(key))) {
            return cls.cast(this.dataBundle.get(key));
        } else {
            Log.d("BIG BAG", "Is not the right TYPE!");
            return null;
        }
    }
    public Boolean exists(String key) {
        return this.dataBundle.containsKey(key);
    }

    public void remove(String key) {
        this.dataBundle.remove(key);
        Log.d("BIG BAG", "Size: " + this.dataBundle.size());
    }
}
