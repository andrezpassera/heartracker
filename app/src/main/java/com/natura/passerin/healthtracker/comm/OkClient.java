package com.natura.passerin.healthtracker.comm;

/**
 * Created by Passerin on 7/24/2015.
 */

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.OkUrlFactory;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import retrofit.client.Request;
import retrofit.client.UrlConnectionClient;

/** Retrofit client that uses OkHttp for communication. */
public class OkClient extends UrlConnectionClient {
    private static OkHttpClient generateDefaultOkHttp() {
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(200 * 1000, TimeUnit.MILLISECONDS);
        // client.setConnectTimeout(Defaults.CONNECT_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);
        client.setReadTimeout(200* 1000, TimeUnit.MILLISECONDS);
        return client;
    }

    private final OkUrlFactory okUrlFactory;

    public OkClient() {
        this(generateDefaultOkHttp());
    }

    public OkClient(OkHttpClient client) {
        this.okUrlFactory = new OkUrlFactory(client);
    }

    @Override
    protected HttpURLConnection openConnection(Request request) throws IOException {
        return okUrlFactory.open(new URL(request.getUrl()));
    }
}