package com.natura.passerin.healthtracker.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.natura.passerin.healthtracker.R;
import com.natura.passerin.healthtracker.activities.HomeActivity;
import com.natura.passerin.healthtracker.activities.LoginActivity;
import com.natura.passerin.healthtracker.comm.RestApi;
import com.natura.passerin.healthtracker.models.History;
import com.natura.passerin.healthtracker.util.Constants;
import com.natura.passerin.healthtracker.util.CustomProgressDialog;
import com.natura.passerin.healthtracker.util.GlobalContext;
import com.natura.passerin.healthtracker.util.Tools;

import org.w3c.dom.Text;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PatientsNameFragment extends Fragment {

    public PatientsNameFragment() {
        // Required empty public constructor
    }

    CustomProgressDialog customProgressDialog;
    GlobalContext mGlobalContext;
    AppCompatActivity mActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_patients_name,
                container, false);
        //instantiates useful variables
        mActivity = (AppCompatActivity) getActivity();
        customProgressDialog = new CustomProgressDialog(mActivity);
        mGlobalContext = (GlobalContext) mActivity.getApplication();

        //inflates name input.
        final EditText etName = (EditText) v.findViewById(R.id.etNombre);
        //inflates accept button
        Button btAccept = (Button) v.findViewById(R.id.btAccept);
        //when accept button is clicked completeness of the view is checked and then request is made.
        btAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String sName = etName.getText().toString();
                if (sName!= null && !sName.equals("")){
                    customProgressDialog.show();
                    mGlobalContext.getRestApi().getHistoryListByName(sName,
                            new Callback<ArrayList<History>>() {
                        @Override
                        public void success(ArrayList<History> histories, Response response) {
                            customProgressDialog.dismiss();
                            if(histories != null) {
                                if (histories.size() > 0) {
                                    //starts home activity if histories <> 0
                                    Intent intent = new Intent(mActivity, HomeActivity.class);
                                    mGlobalContext.put(Constants.USERNAME,sName);
                                    intent.putExtra(Constants.USER, Constants.PATIENT_USER);
                                    startActivity(intent);
                                } else {
                                    Tools.getAlertDialog(mActivity, "Sorry",
                                            "There aren't any records for this patient yet.",
                                            null, null).show();
                                }
                            }else{
                                Toast.makeText(mGlobalContext, Constants.GENERIC_ERROR,
                                        Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            customProgressDialog.dismiss();
                            Toast.makeText(mGlobalContext, Constants.GENERIC_ERROR,
                                    Toast.LENGTH_SHORT).show();
                        }
                    });

                }else{
                    etName.setError("Please complete this field.");
                }
            }
        });
        return v;
    }


}
