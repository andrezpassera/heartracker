package com.natura.passerin.healthtracker.fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.natura.passerin.healthtracker.R;
import com.natura.passerin.healthtracker.models.History;
import com.natura.passerin.healthtracker.util.Constants;
import com.natura.passerin.healthtracker.util.CustomProgressDialog;
import com.natura.passerin.healthtracker.util.GlobalContext;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/*
* Class representing a Fragment in which histories are added by a NURSE.
* */

public class AddHistoryFragment extends Fragment {

    AppCompatActivity mActivity;
    CustomProgressDialog customProgressDialog;
    GlobalContext mGlobalContext;
    Calendar myCalendar = Calendar.getInstance();
    EditText etDate;
    DatePickerDialog.OnDateSetListener date;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_form_history, container, false);

        //initializes useful variables.
        mActivity = (AppCompatActivity) getActivity();
        customProgressDialog = new CustomProgressDialog(mActivity);
        mGlobalContext = (GlobalContext) mActivity.getApplication();

        //initializes every widget value.
        final EditText etName = (EditText) v.findViewById(R.id.etName);
        final EditText etAge = (EditText) v.findViewById(R.id.etAge);
        final EditText etBlood = (EditText) v.findViewById(R.id.etBloodPreassure);
        final EditText etHeart = (EditText) v.findViewById(R.id.etHeartRate);
        etDate = (EditText) v.findViewById(R.id.etDate);

        //builds date input so everytime users touches it. A date picker widget shows up.
        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };
        //sets the click event for the date picker widget
        etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(mActivity, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        //sets the save button event. Every data introduced by the user is checked in here.
        //in case something isn't complete I set an error for the current input affected.
        RelativeLayout rlSave = (RelativeLayout) v.findViewById(R.id.rlSaveRecord);
        rlSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = etName.getText().toString();
                if (name.equals("")){
                    etName.setError(Constants.FIELD_ERROR);
                    return;
                }
                String age = etAge.getText().toString();
                if(age.equals("") ){
                    etAge.setError(Constants.FIELD_ERROR);
                    return;
                }
                if (age.equals("0")){
                    etAge.setError("Please, be serious.");
                    return;
                }
                String blood = etBlood.getText().toString();
                if (blood.equals("")){
                    etBlood.setError(Constants.FIELD_ERROR);
                    return;
                }
                if(!blood.matches("\\d{1,2}\\/\\d{1,2}")){
                    etBlood.setError("Please, use the correct format XX/XX");
                    return;
                }
                String heart = etHeart.getText().toString();
                if (heart.equals("")){
                    etHeart.setError(Constants.FIELD_ERROR);
                    return;
                }
                String date = etDate.getText().toString();
                if (date.equals("")){
                    etDate.setError(Constants.FIELD_ERROR);
                    return;
                }

                //if all requsites are met only the we should allow the user to submit a new entry.
                History hst = new History("",name,age,blood,heart,date);
                customProgressDialog.show();
                mGlobalContext.getRestApi().insertHistory(hst, new Callback<Integer>() {
                    @Override
                    public void success(Integer integer, Response response) {
                        customProgressDialog.dismiss();
                        if (integer == 1){
                            Toast.makeText(mActivity, "Record was successfully saved.", Toast.LENGTH_SHORT).show();
                            getFragmentManager().popBackStack();
                        }else{
                            Toast.makeText(mActivity,Constants.GENERIC_ERROR,Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        customProgressDialog.dismiss();
                        Toast.makeText(mActivity,Constants.GENERIC_ERROR,Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        return v;
    }

    //sets the date picker date format.
    private void updateLabel() {
        String myFormat = "dd/MM/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        etDate.setText(sdf.format(myCalendar.getTime()));
    }
}
