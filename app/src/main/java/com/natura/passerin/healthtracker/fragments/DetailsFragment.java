package com.natura.passerin.healthtracker.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.natura.passerin.healthtracker.R;
import com.natura.passerin.healthtracker.models.History;
import com.natura.passerin.healthtracker.util.Constants;
import com.natura.passerin.healthtracker.util.CustomProgressDialog;
import com.natura.passerin.healthtracker.util.GlobalContext;
import com.natura.passerin.healthtracker.util.Tools;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class DetailsFragment extends Fragment {

    AppCompatActivity mActivity;
    CustomProgressDialog customProgressDialog;
    GlobalContext mGlobalContext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_detail, container, false);
        //sets useful variables
        mActivity = (AppCompatActivity) getActivity();
        customProgressDialog = new CustomProgressDialog(mActivity);
        mGlobalContext = (GlobalContext) mActivity.getApplication();

        //recovers selected item by the user on previous screen.
        final History selectedHist = mGlobalContext.get(Constants.SELECTED_HIST, History.class);
        //sets values for every widget on screen
        TextView tvName = (TextView) v.findViewById(R.id.tvName);
        TextView tvAge = (TextView) v.findViewById(R.id.tvAge);
        TextView tvDate = (TextView) v.findViewById(R.id.tvDate);
        TextView tvBloodPreassure = (TextView) v.findViewById(R.id.tvBlood);
        TextView tvHeartRate = (TextView) v.findViewById(R.id.tvHeart);
        TextView tvBloodPreassureRisk = (TextView) v.findViewById(R.id.tvBloodPreassureRisk);
        TextView tvHeartRisk = (TextView) v.findViewById(R.id.tvHeartRateRisk);
        RelativeLayout btDeleteHistory = (RelativeLayout) v.findViewById(R.id.rlDeleteRecord);
        RelativeLayout btEditHistory = (RelativeLayout) v.findViewById(R.id.rlEditRecord);
        RelativeLayout rlBloodPreassureRisk = (RelativeLayout) v.findViewById(R.id.rlBloodPreassureRisk);
        RelativeLayout rlHeartRateRisk = (RelativeLayout) v.findViewById(R.id.rlHeartRateRisk);

        //checks if given values implies a risk for the user. (bloodpreasure)
        switch (Tools.getBloodPreassureRisk(selectedHist.getBloodPreassure())){
            case Constants.RISK_LOW:
                rlBloodPreassureRisk.setVisibility(View.VISIBLE);
                tvBloodPreassureRisk.setText("Blood preassure below normal levels.");
                break;
            case Constants.RISK_HIGH:
                rlBloodPreassureRisk.setVisibility(View.VISIBLE);
                tvBloodPreassureRisk.setText("Blood preassure above normal levels.");
                break;
        }
        //checks if given values implies a risk for the user. (heartrate)
        switch (Tools.getHeartRateRisk(selectedHist.getHeartRate())){
            case Constants.RISK_LOW:
                rlHeartRateRisk.setVisibility(View.VISIBLE);
                tvHeartRisk.setText("Heart rate below normal levels.");
                break;
            case Constants.RISK_HIGH:
                rlHeartRateRisk.setVisibility(View.VISIBLE);
                tvHeartRisk.setText("Heart rate above normal levels.");
                break;
        }


        if (selectedHist!=null){
            tvName.setText(selectedHist.getName());
            tvAge.setText(selectedHist.getAge() + " years old");
            tvDate.setText(selectedHist.getDate());
            tvBloodPreassure.setText(selectedHist.getBloodPreassure());
            tvHeartRate.setText(selectedHist.getHeartRate());
        }

        //deletes the seleted history
        btDeleteHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customProgressDialog.show();
                mGlobalContext.getRestApi().deleteHistory(selectedHist.getIdHistory(), new Callback<Integer>() {
                    @Override
                    public void success(Integer integer, Response response) {
                        customProgressDialog.dismiss();
                        if (integer == 1){
                            Toast.makeText(mActivity, "Record has been deleted.", Toast.LENGTH_SHORT).show();
                            mGlobalContext.remove(Constants.SELECTED_HIST);
                            getFragmentManager().popBackStack();

                        }else{
                            Toast.makeText(mActivity, Constants.GENERIC_ERROR, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        customProgressDialog.dismiss();
                        Toast.makeText(mActivity, Constants.GENERIC_ERROR, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        //starts new fragment for editing this history record.
        btEditHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                EditHistoryFragment patientsNameFragment = new EditHistoryFragment();
                ft.setCustomAnimations(R.anim.right_to_left_in, R.anim.left_to_right_out, R.anim.left_to_right_in, R.anim.right_to_left_out);
                ft.replace(R.id.container, patientsNameFragment);
                ft.addToBackStack(null);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.commit();
            }
        });
        return v;
    }


}
