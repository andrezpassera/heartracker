package com.natura.passerin.healthtracker.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.natura.passerin.healthtracker.R;
import com.natura.passerin.healthtracker.models.History;
import com.natura.passerin.healthtracker.util.Constants;
import com.natura.passerin.healthtracker.util.Tools;

import java.util.ArrayList;

/**
 * Created by Andres on 20/08/2015.
 */
public abstract class PatientHistRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<History> items;
    private Context mContext;

    Activity activity;

    public PatientHistRecyclerAdapter(ArrayList<History> items, Context context){
        this.items = items;
        this.mContext = context;
        this.activity = (Activity) context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflates view for current item.
            this.mContext = parent.getContext();
            View v = LayoutInflater.from(this.mContext).inflate(R.layout.item_history_patient, parent, false);
            PatientHistoryViewHolder pvh = new PatientHistoryViewHolder(v);
            return pvh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        //sets values for current item from object in list.
        final History item = this.items.get(position);
        if (item!=null) {
            PatientHistoryViewHolder mHolder = (PatientHistoryViewHolder) holder;
            mHolder.mTvDate.setText(item.getDate());
            mHolder.mRvDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClick(item);
                }
            });
            mHolder.mTvBlood.setText(item.getBloodPreassure());
            mHolder.mTvHeart.setText(item.getHeartRate());
            if (!Tools.getBloodPreassureRisk(item.getBloodPreassure()).
                    equals(Constants.RISK_NOT) || !Tools.getHeartRateRisk(item.getHeartRate()).
                    equals(Constants.RISK_NOT)){
                mHolder.mRisk.setVisibility(View.VISIBLE);
            }else{
                mHolder.mRisk.setVisibility(View.GONE);
            }
        }
    }



    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
            return this.items.size();
    }

    public abstract void onItemClick(History item);

    public static class PatientHistoryViewHolder extends RecyclerView.ViewHolder {
        //sets view holder for perfomance improvement.
        TextView mTvBlood;
        TextView mTvHeart;
        TextView mTvDate;
        LinearLayout mRvDetails;
        RelativeLayout mRisk;

        PatientHistoryViewHolder(View itemView) {
            super(itemView);
            mRvDetails = (LinearLayout) itemView.findViewById(R.id.body);
            mTvDate = (TextView) itemView.findViewById(R.id.tvDate);
            mTvBlood = (TextView) itemView.findViewById(R.id.tvBlood);
            mTvHeart = (TextView) itemView.findViewById(R.id.tvHeart);
            mRisk = (RelativeLayout) itemView.findViewById(R.id.rlRisk);
        }
    }
}


