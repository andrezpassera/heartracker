package com.natura.passerin.healthtracker.comm;

/**
 * Created by Passerin on 10/24/2018.
 */
public class CommReq {
    //global constants for use in the app.
    public static final String BASE_URL = "http://68.183.111.195:8080/healthtracker";
//    public static final String BASE_URL = "http://192.168.0.5:8080/healthtracker";
//    public static final String BASE_URL = "http://192.168.67.152:8080/healthtracker";
    public static final String GET_ALL_HISTORIES = "/history/list";
    public static final String GET_ALL_HISTORIES_BY_NAME = "/history/list/{name}";
    public static final String INSERT_HISTORY = "/history/insert";
    public static final String EDIT_HISTORY = "/history/edit";
    public static final String DELETE_HISTORY = "/history/delete/{idHistory}";
}

