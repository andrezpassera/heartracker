package com.natura.passerin.healthtracker.fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.natura.passerin.healthtracker.R;
import com.natura.passerin.healthtracker.models.History;
import com.natura.passerin.healthtracker.util.Constants;
import com.natura.passerin.healthtracker.util.CustomProgressDialog;
import com.natura.passerin.healthtracker.util.GlobalContext;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class EditHistoryFragment extends Fragment {

    AppCompatActivity mActivity;
    CustomProgressDialog customProgressDialog;
    GlobalContext mGlobalContext;
    Calendar myCalendar = Calendar.getInstance();
    EditText etDate;
    DatePickerDialog.OnDateSetListener date;
    EditText etName ;
    EditText etAge;
    EditText etBlood;
    EditText etHeart;
    TextView btTitle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_form_history, container, false);
        mActivity = (AppCompatActivity) getActivity();
        customProgressDialog = new CustomProgressDialog(mActivity);
        mGlobalContext = (GlobalContext) mActivity.getApplication();

        etName = (EditText) v.findViewById(R.id.etName);
        etAge = (EditText) v.findViewById(R.id.etAge);
        etBlood = (EditText) v.findViewById(R.id.etBloodPreassure);
        etHeart = (EditText) v.findViewById(R.id.etHeartRate);
        etDate = (EditText) v.findViewById(R.id.etDate);
        btTitle = (TextView) v.findViewById(R.id.btSave);
        btTitle.setText("Update Record");
        final History hstOriginal = mGlobalContext.get(Constants.SELECTED_HIST,History.class);
        etName.setText(hstOriginal.getName());
        etAge.setText(hstOriginal.getAge());
        etBlood.setText(hstOriginal.getBloodPreassure());
        etHeart.setText(hstOriginal.getHeartRate());
        etDate.setText(hstOriginal.getDate());

        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(mActivity, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        //checks every value in the form.
        RelativeLayout rlSave = (RelativeLayout) v.findViewById(R.id.rlSaveRecord);
        rlSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = etName.getText().toString();
                if (name.equals("")){
                    etName.setError(Constants.FIELD_ERROR);
                    return;
                }
                String age = etAge.getText().toString();
                if(age.equals("")){
                    etAge.setError(Constants.FIELD_ERROR);
                    return;
                }
                if (age.equals("0")){
                    etAge.setError("Please, be serious.");
                    return;
                }
                String blood = etBlood.getText().toString();
                if (blood.equals("")){
                    etBlood.setError(Constants.FIELD_ERROR);
                    return;
                }
                if(!blood.matches("\\d{1,2}\\/\\d{1,2}")){
                    etBlood.setError("Please, use the correct format XX/XX");
                    return;
                }
                String heart = etHeart.getText().toString();
                if (heart.equals("")){
                    etHeart.setError(Constants.FIELD_ERROR);
                    return;
                }
                String date = etDate.getText().toString();
                if (date.equals("")){
                    etDate.setError(Constants.FIELD_ERROR);
                    return;
                }
                History hst = new History(hstOriginal.getIdHistory(),name,age,blood,heart,date);
                customProgressDialog.show();
                //only if all form data is ok the we can send the edit request.
                mGlobalContext.getRestApi().editHistory(hst, new Callback<Integer>() {
                    @Override
                    public void success(Integer integer, Response response) {
                        customProgressDialog.dismiss();
                        if (integer == 1){
                            Toast.makeText(mActivity, "Record was successfully updated.", Toast.LENGTH_SHORT).show();
                            FragmentTransaction ft = getFragmentManager().beginTransaction();
                            if (mGlobalContext.get(Constants.USER,String.class).equals(Constants.NURSE_USER)) {
                                //starts nurses home if is the current user
                                NurseHomeFragment nurseHomeFragment = (NurseHomeFragment) getFragmentManager().findFragmentByTag("HOME");
                                ft.setCustomAnimations(R.anim.right_to_left_in, R.anim.left_to_right_out, R.anim.left_to_right_in, R.anim.right_to_left_out);
                                ft.replace(R.id.container, nurseHomeFragment);
                                ft.addToBackStack(null);
                                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                                ft.commit();
                            }else{
                                //starts patients home if is the current user
                                PatientHomeFragment nurseHomeFragment = (PatientHomeFragment) getFragmentManager().findFragmentByTag("HOME");
                                ft.setCustomAnimations(R.anim.right_to_left_in, R.anim.left_to_right_out, R.anim.left_to_right_in, R.anim.right_to_left_out);
                                ft.replace(R.id.container, nurseHomeFragment);
                                ft.addToBackStack(null);
                                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                                ft.commit();
                            }
                        }else{
                            Toast.makeText(mActivity,Constants.GENERIC_ERROR,Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        customProgressDialog.dismiss();
                        Toast.makeText(mActivity,Constants.GENERIC_ERROR,Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        return v;
    }
    //sets the date picker data format
    private void updateLabel() {
        String myFormat = "dd/MM/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        etDate.setText(sdf.format(myCalendar.getTime()));
    }


}
