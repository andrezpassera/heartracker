package com.natura.passerin.healthtracker.activities;

import android.content.Intent;
import android.media.Image;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.natura.passerin.healthtracker.R;
import com.natura.passerin.healthtracker.adapters.NurseHistRecyclerAdapter;
import com.natura.passerin.healthtracker.fragments.LoginFragment;
import com.natura.passerin.healthtracker.fragments.NurseHomeFragment;
import com.natura.passerin.healthtracker.fragments.PatientHomeFragment;
import com.natura.passerin.healthtracker.models.History;
import com.natura.passerin.healthtracker.util.Constants;
import com.natura.passerin.healthtracker.util.CustomProgressDialog;
import com.natura.passerin.healthtracker.util.GlobalContext;
import com.natura.passerin.healthtracker.util.Tools;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class HomeActivity extends AppCompatActivity {

    GlobalContext mGlobalContext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        //initializes global context.
        mGlobalContext = (GlobalContext) getApplication();
        //sets toolbar title
        Toolbar tb = (Toolbar) findViewById(R.id.tbHome);
        TextView tvTitle = (TextView) tb.findViewById(R.id.tvTitle);
        //sets back button
        ImageView ivBack = (ImageView) tb.findViewById(R.id.btBack);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               onBackPressed();
            }
        });

        //gets patient info so it knows if the app is used by a patient or by a nurse
        Intent extras = getIntent();
        String userType = Constants.NURSE_USER;
        String userName = "";
        if(extras!=null){
            userType = extras.getStringExtra(Constants.USER);
            userName = mGlobalContext.get(Constants.USERNAME,String.class);
        }
        //selects HOME fragment depending on user type
        switch (userType){
            case Constants.NURSE_USER:
                //Start home Fragment instance
                tvTitle.setText("Last entries");
                NurseHomeFragment fragmentNurseHome = new NurseHomeFragment();
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.add(R.id.container, fragmentNurseHome, "HOME");
                ft.setCustomAnimations(R.anim.fade_out, R.anim.fade_in);
                ft.addToBackStack(null);
                ft.commit();
                break;
            case Constants.PATIENT_USER:
                //Start home Fragment instance
                tvTitle.setText("Welcome back, " + userName);
                PatientHomeFragment fragmentNurseHome1 = new PatientHomeFragment();
                final FragmentTransaction ft1 = getSupportFragmentManager().beginTransaction();
                ft1.add(R.id.container, fragmentNurseHome1, "HOME");
                ft1.setCustomAnimations(R.anim.fade_out, R.anim.fade_in);
                ft1.addToBackStack(null);
                ft1.commit();
                break;
        }

    }

    @Override
    public void onBackPressed() {
        //kills activity only if current visibble fragment is HOME.
       Fragment fragment =  getSupportFragmentManager().findFragmentByTag("HOME");
       if (fragment!=null && fragment.isVisible()){
           finish();
       }
       super.onBackPressed();
    }
}
