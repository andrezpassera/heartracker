package com.natura.passerin.healthtracker.comm;

import android.telecom.Call;

import com.natura.passerin.healthtracker.models.History;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;


/**
 * Created by Passerin on 7/24/2015.
 *
 * Interface created for http rest communications using the retrofit library.
 * each of the requests built are for use in the whole app.
 */
public interface RestApi {

    @GET(CommReq.GET_ALL_HISTORIES)
    void getHistoryList(
            Callback<ArrayList<History>> callback
    );

    @GET(CommReq.GET_ALL_HISTORIES_BY_NAME)
    void getHistoryListByName(
            @Path("name") String name,
            Callback<ArrayList<History>> callback
    );

    @POST(CommReq.INSERT_HISTORY)
    void insertHistory(
            @Body History hst,
            Callback<Integer> callback
    );

    @POST(CommReq.EDIT_HISTORY)
    void editHistory(
            @Body History hst,
            Callback<Integer> callback
    );

    @GET(CommReq.DELETE_HISTORY)
    void deleteHistory(
            @Path("idHistory") String idHistory,
            Callback<Integer> callback
    );

}
