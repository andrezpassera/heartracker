package com.natura.passerin.healthtracker.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by passerin on 10/22/18.
 */

public class Tools {

    public static final String TAGNAME = "Health Tracker : ";

    //Logger with all tags set
    public static class Logger {
        public static void d(String message) {
            Log.d(TAGNAME, message);
        }

        public static void d(Bundle bundle) {
            String message = "Bundle{\n";
            for (String key : bundle.keySet()) {
                message += "  " + key + ": " + bundle.get(key) + "\n";
            }
            message += "}";
            Log.d(TAGNAME, message);
        }

        public static void e(String message) {
            Log.e(TAGNAME, message);
        }

        public static void e(Throwable throwable) {
            Log.e(TAGNAME, "ErrorMsg", throwable);
        }

        public static void i(String message) {
            Log.i(TAGNAME, message);
        }

        public static void w(String message) {
            Log.w(TAGNAME, message);
        }
    }

    public static AlertDialog.Builder getAlertDialog(final Context context,
                                                     final String title, String text, final DialogInterface.OnCancelListener listener,
                                                     final String errCode) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle(title);
        dialog.setCancelable(false);
        dialog.setMessage(text);

        dialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //what happens if user clicks ACCEPT.
            }
        });
        return dialog;
    }

    public static String getBloodPreassureRisk(String bloodPreassure) {
        String[] parts = bloodPreassure.split("/");
        if (parts != null) {
            String part1 = parts[0]; // 004
            String part2 = parts[1]; // 034556
            if (part1 != null) {
                if (Integer.parseInt(part1) < 8 || Integer.parseInt(part2) < 6) {
                    return Constants.RISK_LOW;
                } else if (Integer.parseInt(part1) == 12 && Integer.parseInt(part2) == 8) {
                    return Constants.RISK_NOT;
                } else if (Integer.parseInt(part1) > 12 || Integer.parseInt(part2) > 8) {
                    return Constants.RISK_HIGH;
                }
            }
        }
        return Constants.RISK_NOT;
    }

    public static String getHeartRateRisk(String heartRate){
        if(heartRate!=null){
            if (Integer.parseInt(heartRate) > 100){
                return Constants.RISK_HIGH;
            }else if(Integer.parseInt(heartRate) < 60){
                return Constants.RISK_LOW;
            }else{
                return Constants.RISK_NOT;
            }
        }
        return Constants.RISK_NOT;
    }
}
