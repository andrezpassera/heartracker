package com.natura.passerin.healthtracker.adapters;

import android.animation.Animator;
import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.natura.passerin.healthtracker.R;
import com.natura.passerin.healthtracker.models.History;

import java.util.ArrayList;

/**
 * Created by Andres on 20/08/2015.
 */
public abstract class NurseHistRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<History> items;
    private Context mContext;

    Activity activity;

    public NurseHistRecyclerAdapter(ArrayList<History> items, Context context){
        //initializes values for this instance of adapter
        this.items = items;
        this.mContext = context;
        this.activity = (Activity) context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            this.mContext = parent.getContext();
            //inflates view for current item.
            View v = LayoutInflater.from(this.mContext).inflate(R.layout.item_history_nurse, parent, false);
            NurseHistoryViewHolder pvh = new NurseHistoryViewHolder(v);
            return pvh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        final History item = this.items.get(position);
        if (item!=null) {
            //sets values for current item from object in list.
            NurseHistoryViewHolder mHolder = (NurseHistoryViewHolder) holder;
            mHolder.mTvName.setText(item.getName());
            mHolder.mTvDate.setText(item.getDate());
            mHolder.mRvDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClick(item);
                }
            });
        }
    }



    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
            return this.items.size();
    }

    public abstract void onItemClick(History item);

    public static class NurseHistoryViewHolder extends RecyclerView.ViewHolder {
        //sets view holder for perfomance improvement.
        TextView mTvName;
        TextView mTvDate;
        RelativeLayout mRvDetails;

        NurseHistoryViewHolder(View itemView) {
            super(itemView);
            mRvDetails = (RelativeLayout) itemView.findViewById(R.id.rlItem);
            mTvName = (TextView) itemView.findViewById(R.id.tvName);
            mTvDate = (TextView) itemView.findViewById(R.id.tvDate);
        }
    }
}


