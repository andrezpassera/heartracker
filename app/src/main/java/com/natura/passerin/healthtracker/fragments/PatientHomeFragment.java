package com.natura.passerin.healthtracker.fragments;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.natura.passerin.healthtracker.R;
import com.natura.passerin.healthtracker.adapters.NurseHistRecyclerAdapter;
import com.natura.passerin.healthtracker.adapters.PatientHistRecyclerAdapter;
import com.natura.passerin.healthtracker.models.History;
import com.natura.passerin.healthtracker.util.Constants;
import com.natura.passerin.healthtracker.util.CustomProgressDialog;
import com.natura.passerin.healthtracker.util.GlobalContext;
import com.natura.passerin.healthtracker.util.Tools;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PatientHomeFragment extends Fragment {

    ArrayList<History> mHistories = new ArrayList<History>();
    PatientHistRecyclerAdapter mAdapter;
    AppCompatActivity mActivity;
    CustomProgressDialog customProgressDialog;
    GlobalContext mGlobalContext;
    TextView tvEmpty;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_patient_home, container, false);

        //instantiates useful variables
        mActivity = (AppCompatActivity) getActivity();
        customProgressDialog = new CustomProgressDialog(mActivity);
        mGlobalContext = (GlobalContext) mActivity.getApplication();

        //inflates empty message just in case list is empty
        tvEmpty = (TextView) v.findViewById(R.id.tvEmpty);
        //inflates recycler view widget
        RecyclerView rvRecycler = (RecyclerView) v.findViewById(R.id.rvRecycler);
        rvRecycler.setHasFixedSize(true);
        //sets linear layout for recycler view widget
        final LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvRecycler.setLayoutManager(layoutManager);
        //adapter for patients recycler view.
        mAdapter= new PatientHistRecyclerAdapter(mHistories,mActivity) {
            @Override
            public void onItemClick(History item) {
                mGlobalContext.put(Constants.SELECTED_HIST,item);
                //when an item is clicked user is taken to the details fragment.
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                DetailsFragment detailsFragment = new DetailsFragment();
                ft.setCustomAnimations(R.anim.right_to_left_in, R.anim.left_to_right_out, R.anim.left_to_right_in, R.anim.right_to_left_out);
                ft.addToBackStack(null);
                ft.replace(R.id.container, detailsFragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.commit();
            }
        };

        rvRecycler.setAdapter(mAdapter);
        customProgressDialog.show();
        //retrieves history list from server. by name.
        mGlobalContext.getRestApi().getHistoryListByName(mGlobalContext.get(Constants.USERNAME,
                String.class),new Callback<ArrayList<History>>() {
            @Override
            public void success(ArrayList<History> history, Response response) {
                customProgressDialog.dismiss();
                //updates recycler view widget.
                if(history.size() > 0) {
                    tvEmpty.setVisibility(View.GONE);
                }else{
                    tvEmpty.setVisibility(View.VISIBLE);
                }
                //clears histories list.
                mHistories.clear();
                //adds new data.
                mHistories.addAll(history);
                //notifies adapter
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError error) {
                customProgressDialog.dismiss();
                Tools.Logger.e(error.toString());
            }
        });



        return v;
    }


}
