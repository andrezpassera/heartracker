package com.natura.passerin.healthtracker.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.natura.passerin.healthtracker.R;
import com.natura.passerin.healthtracker.activities.HomeActivity;
import com.natura.passerin.healthtracker.util.Constants;
import com.natura.passerin.healthtracker.util.CustomProgressDialog;
import com.natura.passerin.healthtracker.util.GlobalContext;

public class LoginFragment extends Fragment {


    AppCompatActivity mActivity;
    CustomProgressDialog customProgressDialog;
    GlobalContext mGlobalContext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_login, container, false);
        RelativeLayout rlPatient = (RelativeLayout) v.findViewById(R.id.rlPatient);
        RelativeLayout rlNurse = (RelativeLayout) v.findViewById(R.id.rlNurse);

        mActivity = (AppCompatActivity) getActivity();
        customProgressDialog = new CustomProgressDialog(mActivity);
        mGlobalContext = (GlobalContext) mActivity.getApplication();
        //if patient's selected then patient's name fragment should be shown.
        rlPatient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mGlobalContext.put(Constants.USER,Constants.PATIENT_USER);
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                PatientsNameFragment patientsNameFragment = new PatientsNameFragment();
                ft.setCustomAnimations(R.anim.right_to_left_in, R.anim.left_to_right_out, R.anim.left_to_right_in, R.anim.right_to_left_out);
                ft.addToBackStack(null);
                ft.replace(R.id.container, patientsNameFragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.commit();
            }
        });
        //if nurse's selected then nurses home fragment should be shown
        rlNurse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mGlobalContext.put(Constants.USER,Constants.NURSE_USER);
                Intent intent = new Intent(getActivity(), HomeActivity.class);
                intent.putExtra(Constants.USER,Constants.NURSE_USER);
                startActivity(intent);
            }
        });

        return v;
    }


}
